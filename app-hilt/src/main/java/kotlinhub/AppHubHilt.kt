package kotlinhub

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class AppHubHilt : Application()