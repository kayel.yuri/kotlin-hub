package kotlinhub.view

import base.ItemViewBuilder
import kotlinfun.hub.hilt.databinding.ItemRepoPullsBinding
import kotlinhub.get
import kotlinhub.loadURL
import model.ResponsePulls

class ItemRepoPullsBuilder :
    ItemViewBuilder<ResponsePulls.Pull, ItemRepoPullsBinding>(ItemRepoPullsBinding::class) {

    override fun ItemRepoPullsBinding.onBind(position: Int) {
        collection.get(position).run {
            pullTitle.text = title
            pullUser.text = user.login
            val created = "created at $createdAt"
            pullTimeStamp.text = created
            pullDescription.text = body
            pullPhoto.loadURL(user.avatarUrl)
            pullBg.loadURL(user.avatarUrl)
        }
    }
}