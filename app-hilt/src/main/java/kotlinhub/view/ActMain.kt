package kotlinhub.view

import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.core.os.bundleOf
import base.ActBind
import base.IActContract
import base.Steve.main
import base.loadFrag
import base.buildRecyclerAdapter
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.snackbar.Snackbar.LENGTH_LONG
import dagger.hilt.android.AndroidEntryPoint
import kotlinfun.hub.hilt.databinding.ActMainBinding
import kotlinhub.get
import kotlinhub.stringAny
import kotlinhub.view.FragDetail.Companion.KEY_CREATOR
import kotlinhub.view.FragDetail.Companion.KEY_REPO
import kotlinhub.viewmodel.ViewModelMain
import model.ResponseRepos.Repo
import javax.inject.Inject

@AndroidEntryPoint
class ActMain : ActBind<ActMainBinding>(ActMainBinding::class), IActContract {

    @Inject
    lateinit var viewModel: ViewModelMain

    private val repoSet = mutableSetOf<Repo>()

    override fun ActMainBinding.onBoundView() {
        val adapter = actMainRecycler.buildRecyclerAdapter<ItemRepoBuilder>(repoSet)

        adapter.onTarget = { viewModel.getRepos() }

        viewModel.getRepos()

        viewModel.repoSet.observe(this@ActMain, {
            repoSet.addAll(it)
            adapter.notifyDataSetChanged()
        })

        viewModel.loading.observe(this@ActMain, { loading ->
            defaultProgressBar.visibility = if (loading) VISIBLE else GONE
        })

        viewModel.message.observe(this@ActMain, { message ->
            Snackbar.make(
                root,
                "${stringAny(message.first)}\n${stringAny(message.second)}",
                LENGTH_LONG
            ).show()
        })
    }

    override fun onResume() {
        super.onResume()
        viewModel.getRepos()
    }

    override fun setLoading(loading: Boolean) {
        main {
            binding.defaultProgressBar.visibility = if (loading) VISIBLE else GONE
        }
    }

    override fun onItemClick(id: Int) {
        val repo = repoSet.get(id)
        loadFrag<FragDetail>(
            bundleOf(
                KEY_CREATOR to repo.owner.login,
                KEY_REPO to repo.name
            )
        )
    }
}

