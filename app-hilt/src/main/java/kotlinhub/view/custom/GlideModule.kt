package kotlinhub.view.custom

import android.content.Context
import com.bumptech.glide.GlideBuilder
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.load.engine.cache.LruResourceCache
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class GlideModule : AppGlideModule() {

    override fun applyOptions(context: Context, builder: GlideBuilder) {
        val memoryCacheSizeBytes: Long = 1024 * 1024 * 100 // 100mb
        builder.setMemoryCache(LruResourceCache(memoryCacheSizeBytes))
    }
}