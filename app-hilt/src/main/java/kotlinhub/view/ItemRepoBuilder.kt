package kotlinhub.view

import base.IActContract
import base.ItemViewBuilder
import kotlinfun.hub.hilt.databinding.ItemRepoBinding
import kotlinhub.get
import kotlinhub.loadURL
import model.ResponseRepos

class ItemRepoBuilder : ItemViewBuilder<ResponseRepos.Repo, ItemRepoBinding>(ItemRepoBinding::class) {

    override fun ItemRepoBinding.onBind(position: Int) {
        collection.get(position).run {
            repoUsername.text = owner.login
            repoFullname.text = fullName
            repoDescription.text = description
            repoStars.text = stargazersCount.toString()
            repoForks.text = forksCount.toString()
            repoPhoto.loadURL(owner.avatarUrl)
            repoBg.loadURL(owner.avatarUrl)
        }
        root.setOnClickListener { (activity as IActContract).onItemClick(position) }
    }
}