package kotlinhub.view

import android.graphics.Color.TRANSPARENT
import android.graphics.Color.WHITE
import android.os.Bundle
import androidx.fragment.app.viewModels
import base.IActContract
import base.FragBase
import base.RecyclerAdapter
import kotlinhub.repository.RepoGIT
import base.Show.alert
import base.buildRecyclerAdapter
import kotlinhub.view.custom.VerticalRecycler
import kotlinhub.viewModelFactory
import kotlinhub.viewmodel.ViewModelDetail
import model.ResponsePulls.Pull

class FragDetail : FragBase<VerticalRecycler>() {

    companion object {
        const val KEY_CREATOR = "creator"
        const val KEY_REPO = "repo"
    }

    private val viewModel: ViewModelDetail by viewModels { requireContext().viewModelFactory<RepoGIT, Void>() }
    private val pullSet = mutableSetOf<Pull>()
    private lateinit var recyclerAdapter: RecyclerAdapter
    override val contentView by lazy {
        VerticalRecycler(requireContext()).apply {
            recyclerAdapter = buildRecyclerAdapter<ItemRepoPullsBuilder>(pullSet)
            setBackgroundColor(TRANSPARENT)
        }
    }

    private var creator = ""
    private var repoName = ""

    override fun Bundle.onArguments() {
        creator = getString(KEY_CREATOR) ?: ""
        repoName = getString(KEY_REPO) ?: ""
    }

    override fun VerticalRecycler.onView() {
        viewModel.getPullRequests(creator, repoName)

        recyclerAdapter.onTarget = { viewModel.getPullRequests(creator, repoName) }

        viewModel.response.observe(viewLifecycleOwner, {
            pullSet.addAll(it)
            contentView.setBackgroundColor(WHITE)
            recyclerAdapter.notifyDataSetChanged()
        })

        viewModel.loading.observe(viewLifecycleOwner, { loading ->
            (activity as IActContract).setLoading(loading)
        })

        viewModel.message.observe(viewLifecycleOwner, { message ->
            alert(message.second) {
                setTitle(message.first)
                setOnDismissListener { (activity as IActContract).backPress() }
            }
        })
    }
}
