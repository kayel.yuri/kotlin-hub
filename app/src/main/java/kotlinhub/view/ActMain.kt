package kotlinhub.view

import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.activity.viewModels
import androidx.core.os.bundleOf
import base.ActBind
import base.IActContract
import base.loadFrag
import base.buildRecyclerAdapter
import kotlinfun.hub.databinding.ActMainBinding
import kotlinhub.get
import kotlinhub.repository.RepoGIT
import kotlinhub.view.FragDetail.Companion.KEY_CREATOR
import kotlinhub.view.FragDetail.Companion.KEY_REPO
import base.Show.snack
import base.Steve.io
import kotlinhub.viewModelFactory
import kotlinhub.viewmodel.ViewModelMain
import model.ResponseRepos.Repo

class ActMain : ActBind<ActMainBinding>(ActMainBinding::class), IActContract {

    private val viewModel: ViewModelMain by viewModels { viewModelFactory<RepoGIT, Void>() }
    private val repoSet = mutableSetOf<Repo>()

    override fun ActMainBinding.onBoundView() {
        val adapter = actMainRecycler.buildRecyclerAdapter<ItemRepoBuilder>(repoSet)

        adapter.onTarget = { viewModel.getRepos() }

        viewModel.getRepos()

        viewModel.repoSet.observe(this@ActMain, {
            repoSet.addAll(it)
            adapter.notifyDataSetChanged()
        })

        viewModel.loading.observe(this@ActMain, { loading ->
            defaultProgressBar.visibility = if (loading) VISIBLE else GONE
        })

        viewModel.message.observe(this@ActMain, { message ->
            snack(message)
        })
    }

    override fun onResume() {
        super.onResume()
        io {
            viewModel.getRepos()
        }
    }

    override fun setLoading(loading: Boolean) {
        binding.defaultProgressBar.visibility = if (loading) VISIBLE else GONE
    }

    override fun onItemClick(id: Int) {
        val repo = repoSet.get(id)
        loadFrag<FragDetail>(
            bundleOf(
                KEY_CREATOR to repo.owner.login,
                KEY_REPO to repo.name
            )
        )
    }
}

