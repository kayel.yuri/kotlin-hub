package kotlinhub.singleton

object Duration {

    const val LENGTH_LONG_MILLIS : Long = 3500
    const val LENGTH_SHORT_MILLIS : Long = 2000

}