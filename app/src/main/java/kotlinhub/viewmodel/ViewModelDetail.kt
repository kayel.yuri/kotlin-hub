package kotlinhub.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import base.Steve.main
import kotlinfun.hub.R
import kotlinhub.get
import kotlinhub.repository.RepoGIT
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import model.ResponsePulls
import java.lang.Exception

@Suppress("UNUSED_PARAMETER")
class ViewModelDetail(appContext: Context, private val repoGIT: RepoGIT) : ViewModel() {

    val message = MutableLiveData<Pair<Int, Any>>()
    val loading = MutableLiveData<Boolean>()
    val response = MutableLiveData<ResponsePulls>()

    private var page = 1

    private lateinit var exception: Exception
    val exceptionThrown get() = R.string.oops to (exception.message ?: R.string.try_later)
    val noPullsToFetch = R.string.no_pulls_title to R.string.no_pulls_body

    fun getPullRequests(creator: String, repo: String) = main {
        try {
            loading.value = true
            val result = repoGIT.getPullRequests(creator, repo, page)
            if (result.size > 0) {
                response.postValue(result)
                if (result.get(0).base.repo.hasPages) {
                    page += 1
                }
            } else if (response.value == null || response.value?.size == 0) {
                message.postValue(noPullsToFetch)
            }
        } catch (ex: Exception) {
            exception = ex
            message.postValue(exceptionThrown)
        } finally {
            loading.postValue(false)
        }
    }
}
