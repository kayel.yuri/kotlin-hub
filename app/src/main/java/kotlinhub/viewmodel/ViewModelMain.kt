package kotlinhub.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import base.Steve.io
import base.Steve.main
import kotlinfun.hub.R
import kotlinhub.repository.RepoGIT
import model.ResponseRepos
import model.ResponseRepos.Repo
import java.lang.Exception

@Suppress("UNUSED_PARAMETER")
class ViewModelMain(appContext: Context, private val repoGIT: RepoGIT) : ViewModel() {

    val message = MutableLiveData<Pair<Int, Any>>()
    val loading = MutableLiveData<Boolean>()
    val repoSet = MutableLiveData<Set<Repo>>()
    val response = MutableLiveData<ResponseRepos>()

    //  Only the first 1000 search results are available https://docs.github.com/v3/search/
    var page = 1
    val pageLimit = 33

    private lateinit var exception: Exception
    val exceptionThrown get() = R.string.oops to (exception.message ?: R.string.try_later)
    val limitReached = R.string.result_limit_title to R.string.results_limit_body

    fun getRepos() = main {
        try {
            loading.value = true
            if (page <= pageLimit) {
                val result = repoGIT.getRepo(page)
                response.postValue(result)
                repoSet.postValue(result.repos)
                page += 1
            } else {
                message.postValue(limitReached)
            }
        } catch (ex: Exception) {
            exception = ex
            message.postValue(exceptionThrown)
        } finally {
            loading.postValue(false)
        }
    }
}
