package kotlinhub

import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.*

fun <T> Iterable<T>.get(index: Int): T {
    forEachIndexed { indexed, element -> if (indexed == index) return element }
    throw IndexOutOfBoundsException()
}

fun <T> MutableCollection<T>.plus(element: T): MutableCollection<T> {
    add(element)
    return this
}

fun ImageView.loadURL(url: String) =
    Glide.with(this).load(url).into(this)

fun View.stringAny(text: Any?): CharSequence = context.stringAny(text)

fun Context.stringAny(text: Any?): String = when (text) {
    is String -> text
    is CharSequence -> text.toString()
    is Pair<*, *> -> stringAny(text.first) + " " + stringAny(text.second)
    is Int -> getResourceOrToString(text)
    else -> ""
}

fun Context.getResourceOrToString(text: Int) = try {
    getString(text)
} catch (ex: Resources.NotFoundException) {
    text.toString()
}

inline fun <reified X, reified Y> Context.viewModelFactory() =
    object : ViewModelProvider.Factory {

        val x = X::class.java
        val y = Y::class.java
        val c = Context::class.java
        val context = this@viewModelFactory.applicationContext

        override fun <Model : ViewModel?> create(clazz: Class<Model>) =
            clazz.getConstructor(c, x).newInstance(
                context,
                if (y == Void::class.java) {
                    x.getConstructor(c).newInstance(context)
                } else {
                    x.getConstructor(c, y).newInstance(context, y.newInstance())
                }
            )
    }

val Activity.contentView get() = findViewById<ViewGroup>(android.R.id.content).getChildAt(0)

