package kotlinhub.remote

import model.ResponsePulls
import model.ResponseRepos
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import java.lang.Exception

interface ServiceGIT : RetroService {

    @GET("search/repositories?q=language:Kotlin&sort=stars")
    @Throws(Exception::class)
    suspend fun getRepo(
        @Query("page") page: Int = 1
    ): ResponseRepos

    @GET("repos/{creator}/{repository}/pulls")
    @Throws(Exception::class)
    suspend fun getPullRequests(
        @Path("creator") creator: String,
        @Path("repository") repo: String,
        @Query("page") page: Int = 1
    ): ResponsePulls
}

interface ServicePlaneWalker : RetroService {

    @GET("search/repositories?q=language:Kotlin&sort=stars")
    @Throws(Exception::class)
    suspend fun getPlane(
        @Query("page") page: Int = 1
    ): ResponseRepos

    @GET("repos/{creator}/{repository}/pulls")
    @Throws(Exception::class)
    suspend fun getWalker(
        @Path("creator") creator: String,
        @Path("repository") repo: String,
        @Query("page") page: Int = 1
    ): ResponsePulls
}