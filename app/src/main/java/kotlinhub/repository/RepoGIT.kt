package kotlinhub.repository

import android.content.Context
import base.Steve.io
import kotlinhub.remote.RetroAuth
import kotlinhub.remote.RetroCache
import kotlinhub.remote.RetroConnect
import kotlinhub.remote.RetroRepository
import kotlinhub.remote.ServiceGIT
import kotlinhub.remote.ServicePlaneWalker
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext
import java.lang.Exception

//  Temporary GitHub App credentials deliberately public
const val USER = "aa3822c4ece4a75e5ed4"
const val PASS = "f84efaec7a7dd194f749f639b7e3f462d8444f5e"
//  Credenciais temporárias de App do GitHub deliberadamente públicas

class RepoGIT(appContext: Context) : RetroRepository<ServiceGIT>(
    RetroConnect(appContext, "https://api.github.com", ServiceGIT::class),
    RetroAuth(USER, PASS)
) {

    @Throws(Exception::class)
    suspend fun getRepo(page: Int = 1) =
            service.getRepo(page)

    @Throws(Exception::class)
    suspend fun getPullRequests(creator: String, repo: String, page: Int = 1) =
            service.getPullRequests(creator, repo, page)
}