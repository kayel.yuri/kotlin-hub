package kotlinhub.repository

abstract class Repository<Service>{
    abstract val service : Service
}
