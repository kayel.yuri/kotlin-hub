package kotlinhub.viewmodel

import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinhub.get
import kotlinhub.repository.RepoGIT
import model.ResponsePulls
import model.ResponsePulls.Pull
import model.ResponsePulls.Pull.Base
import model.ResponsePulls.Pull.Base.Repo
import org.junit.Test
import java.lang.Exception

class TestViewModelDetail : BaseTest() {

    private val repository = mockk<RepoGIT>()
    private val viewModel = ViewModelDetail(appContext, repository)

    private val emptyResponse = ResponsePulls()
    private val response = ResponsePulls().apply {
        add(
            Pull(
                number = TEST_INT,
                base = Base(repo = Repo(hasPages = true))
            )
        )
    }

    private val creator = javaClass.simpleName
    private val repo = javaClass.name

    @Test
    fun getPulls() {
        viewModel.response.observeForever { assert(it.get(0).number == TEST_INT) }
        coEvery { repository.getPullRequests(creator, repo) } returns response
        viewModel.getPullRequests(creator, repo)
        coVerify { repository.getPullRequests(creator, repo) }
    }

    @Test
    fun getPullsEmptyResponse() {
        viewModel.message.observeForever { assert(it == viewModel.noPullsToFetch) }
        coEvery { repository.getPullRequests(creator, repo) } returns emptyResponse
        viewModel.getPullRequests(creator, repo)
        coVerify { repository.getPullRequests(creator, repo) }
    }

    @Test
    fun getPullsException() {
        viewModel.message.observeForever { assert(it == viewModel.exceptionThrown) }
        coEvery { repository.getPullRequests(creator, repo) } throws Exception()
        viewModel.getPullRequests(creator, repo)
        coVerify { repository.getPullRequests(creator, repo) }
    }
}