package kotlinhub.viewmodel

import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinhub.repository.RepoGIT
import model.ResponseRepos
import org.junit.Test

class TestViewModelMain : BaseTest() {

    private val repository = mockk<RepoGIT>()
    private val viewModel = ViewModelMain(appContext, repository)

    private val response = ResponseRepos(totalCount = TEST_INT)

    @Test
    fun getRepos() {
        viewModel.response.observeForever { assert(it.totalCount == TEST_INT) }
        coEvery { repository.getRepo() } returns response
        viewModel.getRepos()
        coVerify { repository.getRepo() }
    }

    @Test
    fun getReposPageLimitReached() {
        viewModel.message.observeForever { assert(it == viewModel.limitReached) }
        viewModel.page = viewModel.pageLimit + 2
        viewModel.getRepos()
    }

    @Test
    fun getReposException() {
        viewModel.message.observeForever { assert(it == viewModel.exceptionThrown) }
        coEvery { repository.getRepo() } throws Exception()
        viewModel.getRepos()
        coVerify { repository.getRepo() }
    }
}