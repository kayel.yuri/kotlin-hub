package kotlinhub.viewmodel

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.core.app.ApplicationProvider
import org.junit.Rule
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Config.OLDEST_SDK])
abstract class BaseTest {

    @get:Rule
    open val rule = InstantTaskExecutorRule()

    val appContext: Context = ApplicationProvider.getApplicationContext()

    internal companion object {
        const val TEST_INT = 999
        const val TEST_STRING = "test_string"
        const val EMPTY_STRING = ""
    }

}