package kotlinhub

import kotlinhub.repository.RepoGIT
import kotlinhub.viewmodel.ViewModelDetail
import kotlinhub.viewmodel.ViewModelMain
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.module

val hubModules by lazy {
    listOf(
        appContext,
//        *acts,
//        *frags,
        *viewModels,
        *repos
    )
}

private val appContext = module {
    single(named("appContext")) { androidContext() }
}

//private val acts get() = arrayOf()    //  No acts to inject

//private val frags get() = arrayOf()   //  No frag to inject

private val viewModels get() = arrayOf(viewModelMain, viewModelDetail)

private val repos get() = arrayOf(repoGIT)

private val repoGIT = module { single { RepoGIT(get()) } }

private val viewModelMain = module { factory { ViewModelMain(get(), get()) } }

private val viewModelDetail = module { factory { ViewModelDetail(get(), get()) } }