package kotlinhub.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinfun.hub.koin.R
import kotlinhub.repository.RepoGIT
import kotlinx.coroutines.launch
import model.ResponseRepos.Repo
import java.lang.Exception

class ViewModelMain(appContext: Context, private val repoGIT: RepoGIT) : ViewModel() {

    val message = MutableLiveData<Pair<Int, Any>>()
    val loading = MutableLiveData<Boolean>()
    val repoSet = MutableLiveData<Set<Repo>>()

    //  Only the first 1000 search results are available https://docs.github.com/v3/search/
    private var page = 1
    private val pageLimit = 33

    fun getRepos() = viewModelScope.launch {
        try {
            loading.value = true
            if (page <= pageLimit) {
                repoSet.postValue(repoGIT.getRepo(page).repos)
                page += 1
            } else {
                message.postValue(R.string.result_limit_title to R.string.results_limit_body)
            }
        } catch (exception: Exception) {
            message.postValue(R.string.oops to (exception.message ?: R.string.try_later))
        } finally {
            loading.postValue(false)
        }
    }
}
