package kotlinhub.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinfun.hub.koin.R
import kotlinhub.get
import kotlinhub.repository.RepoGIT
import kotlinx.coroutines.launch
import model.ResponsePulls
import java.lang.Exception

class ViewModelDetail(appContext: Context, private val repoGIT: RepoGIT) : ViewModel() {

    val message = MutableLiveData<Pair<Int, Any>>()
    val loading = MutableLiveData<Boolean>()
    val pullSet = MutableLiveData<ResponsePulls>()

    private var page = 1

    fun getPullRequests(creator: String, repo: String) = viewModelScope.launch {
        try {
            loading.value = true
            val result = repoGIT.getPullRequests(creator, repo, page)
            if (result.size > 0) {
                pullSet.postValue(result)
                if (result.get(0).base.repo.hasPages) {
                    page += 1
                }
            } else if (pullSet.value == null || pullSet.value?.size == 0) {
                message.postValue(R.string.no_pulls_title to R.string.no_pulls_body)
            }
        } catch (exception: Exception) {
            message.postValue(R.string.oops to (exception.message ?: R.string.try_later))
        } finally {
            loading.postValue(false)
        }
    }
}
