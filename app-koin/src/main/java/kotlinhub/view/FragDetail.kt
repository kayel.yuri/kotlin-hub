package kotlinhub.view

import android.graphics.Color.WHITE
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import base.IActContract
import base.FragBase
import base.RecyclerAdapter
import base.buildRecyclerAdapter
import kotlinhub.stringAny
import kotlinhub.view.custom.VerticalRecycler
import kotlinhub.viewmodel.ViewModelDetail
import model.ResponsePulls.Pull
import org.koin.androidx.viewmodel.ext.android.viewModel

class FragDetail : FragBase<VerticalRecycler>() {

    companion object {
        const val KEY_CREATOR = "creator"
        const val KEY_REPO = "repo"
    }

    private val viewModel: ViewModelDetail by viewModel()
    private val pullSet = mutableSetOf<Pull>()
    private lateinit var recyclerAdapter : RecyclerAdapter
    override val contentView by lazy {
        VerticalRecycler(requireContext()).apply {
            recyclerAdapter = buildRecyclerAdapter<ItemRepoPullsBuilder>(pullSet)
            setBackgroundColor(WHITE)
        }
    }

    private var creator = ""
    private var repoName = ""

    override fun Bundle.onArguments() {
        creator = getString(KEY_CREATOR) ?: ""
        repoName = getString(KEY_REPO) ?: ""
    }

    override fun VerticalRecycler.onView() {
        viewModel.getPullRequests(creator, repoName)

        recyclerAdapter.onTarget = { viewModel.getPullRequests(creator, repoName) }

        viewModel.pullSet.observe(viewLifecycleOwner, {
            pullSet.addAll(it)
            recyclerAdapter.notifyDataSetChanged()
        })

        viewModel.loading.observe(viewLifecycleOwner, { loading ->
            (activity as IActContract).setLoading(loading)
        })

        viewModel.message.observe(viewLifecycleOwner, { message ->
            AlertDialog.Builder(requireContext())
                .setTitle(message.first)
                .setMessage(requireContext().stringAny(message.second))
                .setOnDismissListener { (activity as IActContract).backPress() }
                .show()
        })
    }
}
