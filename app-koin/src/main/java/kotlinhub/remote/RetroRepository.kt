package kotlinhub.remote

import android.content.Context
import android.content.Context.CONNECTIVITY_SERVICE
import android.net.ConnectivityManager
import android.net.NetworkCapabilities.TRANSPORT_CELLULAR
import android.net.NetworkCapabilities.TRANSPORT_ETHERNET
import android.net.NetworkCapabilities.TRANSPORT_WIFI
import android.os.Build.VERSION.SDK_INT
import android.os.Build.VERSION_CODES.M
import kotlinhub.repository.Repository
import okhttp3.Cache
import okhttp3.CacheControl.Builder
import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit.HOURS
import java.util.concurrent.TimeUnit.MINUTES
import java.util.concurrent.TimeUnit.SECONDS
import kotlin.reflect.KClass

open class RetroRepository<Service : RetroService>(
    connect: RetroConnect<Service>,
    auth: RetroAuth? = null,
    cache: RetroCache = RetroCache()
) : Repository<Service>() {
    override val service: Service = RetroInit(connect, auth, cache).create()
}

class RetroInit<Service : RetroService>(
    private val connect: RetroConnect<Service>,
    private val auth: RetroAuth? = null,
    private val cache: RetroCache = RetroCache()
) {
    companion object {
        private const val AUTH = "Authorization"
        private const val CACHE = "Cache-Control"
        private const val PRAGMA = "Pragma"

        private val converter = GsonConverterFactory.create()

        private val Context.isOnline
            get() =
                (getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager?)?.let {
                    if (SDK_INT >= M) {
                        it.getNetworkCapabilities(it.activeNetwork)?.run {
                            return@let hasTransport(TRANSPORT_CELLULAR) ||
                                    hasTransport(TRANSPORT_WIFI) ||
                                    hasTransport(TRANSPORT_ETHERNET)
                        }
                        false
                    } else {
                        it.activeNetworkInfo?.isConnectedOrConnecting ?: false
                    }
                } ?: false
    }

    private val retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(connect.url)
            .addConverterFactory(converter)
            .client(
                OkHttpClient.Builder().apply {
                    cache(Cache(File(connect.appContext.cacheDir, cache.path), cache.size))
                    readTimeout(15, SECONDS)
                    connectTimeout(10, SECONDS)
                    writeTimeout(10, SECONDS)
                    addNetworkInterceptor(networkInterceptor)
                    addInterceptor(offlineInterceptor)
                    addInterceptor(authInterceptor)
                }.build()
            )
            .build()
    }

    private val networkInterceptor = Interceptor {
        it.proceed(it.request()).newBuilder()
            .removeHeader(PRAGMA)
            .removeHeader(CACHE)
            .header(CACHE, Builder().maxAge(cache.minutesMaxAge, MINUTES).build().toString())
            .build()
    }

    private val offlineInterceptor = Interceptor {
        var request = it.request()
        if (connect.appContext.isOnline) {
            request = request.newBuilder()
                .removeHeader(PRAGMA)
                .removeHeader(CACHE)
                .cacheControl(Builder().maxStale(cache.hoursMaxStale, HOURS).build())
                .build()
        }
        it.proceed(request)
    }

    private val authInterceptor = Interceptor {
        var request = it.request()
        if (auth != null && auth.user.isNotEmpty() && auth.pass.isNotEmpty() && connect.appContext.isOnline) {
            request = request.newBuilder()
                .header(AUTH, Credentials.basic(auth.user, auth.pass))
                .build()
        }
        it.proceed(request)
    }

    fun create(): Service = retrofit.create(connect.service.java)
}

interface RetroService

class RetroConnect<Service : RetroService>(
    val appContext: Context,
    val url: String,
    val service: KClass<Service>
)

class RetroAuth(
    val user: String = "",
    val pass: String = ""
)

class RetroCache(
    val minutesMaxAge: Int = 60,
    val hoursMaxStale: Int = 168,
    val size: Long = 5 * 1024 * 1024,
    val path: String = "responses"
)

